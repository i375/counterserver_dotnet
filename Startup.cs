﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace autocounter
{
    public class Startup
    {
        Object requestQueueSync = new Object();
        const string API_KEY = "qlcUt";

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
//                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                Int32 counterValue = 0;
                // Computing next counter next value in a synced form
                lock (requestQueueSync)
                {
                    var key = context.Request.Query["key"];

                    if (key != API_KEY)
                    {
                        return;
                    }

                    if (File.Exists("counter") == false)
                    {
                        File.Create("counter").Close();
                    }

                    var fileData = File.ReadAllText("counter");

                    try
                    {

                        counterValue = Int32.Parse(fileData);
                    }
                    catch { }

                    counterValue++;

                    File.WriteAllText("counter", counterValue.ToString());
                }


                await context.Response.WriteAsync(counterValue.ToString());
            });
        }
    }
}
